----------------------------------------------------------------------
--
--	ovdeadnomore - An addon that shouldn't have to exist,
--	Warhammer Online: Age of Reckoning
--
--	Author: Ovron <ovron@ovron.com>
--	Version: 1.1
--	Date/Time: 2008-10-20 13:52 CET
--
--	What it does? It removes the count-down box after
--	you have been resurrected... seriously.
--
----------------------------------------------------------------------

ovdeadnomore = {
	version = "1.1";
};

function ovdeadnomore.player_death_cleared()
	for i,v in ipairs(DialogManager.twoButtonDlgs) do
		if(v.inUse == true) then
			if(WStringToString(LabelGetText("TwoButtonDlg"..i.."BoxText")) == "Respawning...") then
				if(WindowGetShowing("TwoButtonDlg"..i)) then
					WindowSetShowing("TwoButtonDlg"..i, false);
				end
			end
		end
	end
end

function ovdeadnomore.initialize()
	RegisterEventHandler(SystemData.Events.PLAYER_DEATH_CLEARED, "ovdeadnomore.player_death_cleared");
	--EA_ChatWindow.Print(L"ovdeadnomore v" .. towstring(ovdeadnomore.version) .. L" (by Ovron of Karak Eight Peaks) has loaded successfully!");
end
