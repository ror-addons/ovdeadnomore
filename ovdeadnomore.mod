<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="ovdeadnomore" version="1.1" date="" >
		<Author name="Ovron" email="ovron.rector@gmail.com" />
		<Description text="An addon that shouldn't have to exist" />

		<Files>
			<File name="ovdeadnomore.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="ovdeadnomore.initialize" />
		</OnInitialize>

		<OnUpdate/>
		<OnShutdown/>
	</UiMod>
</ModuleFile>
